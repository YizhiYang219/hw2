/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import mv.data.DataManager;
import mv.data.Point;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 *
 * @author McKillaGorilla
 */
public class FileManager implements AppFileComponent {

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {

        DataManager dataManager = (DataManager) data;
        dataManager.reset();
        
        JsonObject json = this.loadJSONFile(filePath);

        int numberOfSubregions = Integer.parseInt(json.get("NUMBER_OF_SUBREGIONS").toString());

        int numberOfPolygons;
      
        JsonArray arrayOfSubregions = json.getJsonArray("SUBREGIONS");
      
        
        for (int i = 0; i < arrayOfSubregions.size(); i++) {

            JsonObject outterObject = arrayOfSubregions.getJsonObject(i);

            numberOfPolygons = Integer.parseInt(outterObject.get("NUMBER_OF_SUBREGION_POLYGONS").toString());
            
            
            for (int j = 0; j < 1; j++) {                                               // problem is here. it was numberOfPolygons

                JsonArray middleArray = outterObject.getJsonArray("SUBREGION_POLYGONS");
                
                
                for (int k = 0; k < middleArray.size(); k++) {
                    
                    Polygon polygon = new Polygon();
                    
                    JsonArray innerArray = middleArray.getJsonArray(k);
                    
                    
                    for (int z = 0; z < innerArray.size(); z++) {

                        double x = this.getDataAsDouble(innerArray.getJsonObject(z), "X");
                        double y = this.getDataAsDouble(innerArray.getJsonObject(z), "Y");
                        
                        double relativeX = ((DataManager) data).convertX(x);
                        
                        double relativeY = ((DataManager) data).convertY(y);
                        
                        polygon.getPoints().addAll(relativeX, relativeY);
                        
                    }
                    
                    polygon.setFill(Color.LIGHTGREEN);
                    polygon.setStroke(Color.BLACK);
                    ((DataManager) data).addPolygonToList(polygon);
                }
            }

        }

    }

    public double getDataAsDouble(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber) value;
        return number.bigDecimalValue().doubleValue();
    }

    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber) value;
        return number.bigIntegerValue().intValue();
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

//    public void loadData(AppDataComponent data, String filePath) throws IOException {
//        
//        JsonObject json = this.loadJSONFile(filePath);
//        
//        JsonArray array = json.getJsonArray("SUBREGIONS");
//        
//        JsonObject object = array.getJsonObject(0);
//        
//        JsonArray array2 = object.getJsonArray("SUBREGION_POLYGONS");
//        
//        JsonArray array3 = array2.getJsonArray(0);
//        
//        
//        for(int i = 0; i < array3.size() - 1; i++){
//            
//            double x = this.getDataAsDouble(array3.getJsonObject(i), "X");
//            double y = this.getDataAsDouble(array3.getJsonObject(i), "Y");
//            
//            Point point = new Point(x, y);
//            
//            ((DataManager)data).addToPoints(point);
//        }   
//    }
}
