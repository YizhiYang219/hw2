package mv.data;

import java.util.ArrayList;
import javafx.scene.Node;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Polygon;
import saf.components.AppDataComponent;
import mv.MapViewerApp;
import mv.gui.Workspace;

/**
 *
 * @author McKillaGorilla
 */
public class DataManager implements AppDataComponent {
    MapViewerApp app;
    
    ArrayList<Point> points;
    
    ArrayList<Polygon> polygons;
    
    public DataManager(MapViewerApp initApp) {
        
        app = initApp;
        
        polygons = new ArrayList<Polygon>();
        
    }
    
//    public void addToPoints(Point point){
//        
//        points.add(point);
//    }
    
//    public ArrayList getPoints(){
//        
//        return points;
//    }
    
//    public void addToPolygons(double x, double y){
//        
//        Polygon polygon = new Polygon();
//        
//        double relativeX = (((app.getGUI().getWindow().getWidth()) / 360.0) * (180 + x));
//        double relativeY = (((app.getGUI().getWindow().getWidth()) / 360.0) * (180 + y));
//        
//        polygon.getPoints().addAll(relativeX, relativeY);
//        
//        polygons.add(polygon);
//        
//    }
    
    public ArrayList<Polygon> getPolygons(){
        
        return polygons;
    }
    
    @Override
    public void reset() {
        Workspace pane = (Workspace) app.getWorkspaceComponent();
        pane.changeG(false);
        
        polygons.clear();
    }
    
    
    
    
    public double convertX(double x){
        
        Workspace pane = (Workspace) app.getWorkspaceComponent();
        
        double relativeX = ((app.getGUI().getPrimaryScene().getWidth() / 360.0) * (180 + x));
        
        return relativeX;
    }
    
        public double convertY(double y){
       
        Workspace pane = (Workspace) app.getWorkspaceComponent();
        
        FlowPane flow = (FlowPane)app.getGUI().getAppPane().getTop();
        
        
        double relativeY = (((app.getGUI().getPrimaryScene().getHeight() - flow.getHeight()) / 180) * (90 - y));
        
        return relativeY;
    }
        
        public void addPolygonToList(Polygon polygon){
            
            polygons.add(polygon);
        }
}
