/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.gui;

import java.util.ArrayList;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Scale;
import saf.components.AppWorkspaceComponent;
import mv.MapViewerApp;
import mv.data.DataManager;
import mv.data.Point;
import saf.AppTemplate;
import saf.components.AppFileComponent;
import static saf.settings.AppPropertyType.NEW_ICON;
import static saf.settings.AppPropertyType.NEW_TOOLTIP;
import saf.ui.AppGUI;

/**
 *
 * @author McKillaGorilla
 */
public class Workspace extends AppWorkspaceComponent {

    MapViewerApp app;

    Group root;

    Pane innerPane;

    boolean G = false;

    public Workspace(MapViewerApp initApp) {

        app = initApp;

        workspace = new Pane();

        innerPane = new Pane();

        setupHandler();

        workspace.getChildren().add(innerPane);

    }

    @Override
    public void reloadWorkspace() {

        DataManager data = (DataManager) app.getDataComponent();

        if (this.getInnerPane().getChildren().size() != 0) {

            Pane workspace = app.getWorkspaceComponent().getWorkspace();
            
    

            this.getInnerPane().getChildren().clear();

            workspace.getChildren().remove(0);

            innerPane = new Pane();

            workspace.getChildren().add(innerPane);
        }

        root = new Group();
        ArrayList<Polygon> polygons = data.getPolygons();
        for (int i = 0; i < polygons.size(); i++) {
            innerPane.getChildren().add(polygons.get(i));
        }

        workspace.setStyle("-fx-background-color: lightBlue;");
        
    }

    public void setupHandler() {

        workspace.setOnMousePressed(e -> {

            if (e.isPrimaryButtonDown()) {

                FlowPane flow = (FlowPane) app.getGUI().getAppPane().getTop();

//                innerPane.setTranslateX(innerPane.getTranslateX() + innerPane.getWidth() / 2 - e.getSceneX());
//                innerPane.setTranslateY(innerPane.getTranslateY() + innerPane.getHeight() / 2 + flow.getHeight() - e.getSceneY());

//                Scale scale = new Scale();
//
//                scale.setPivotX(e.getX());
//                scale.setPivotY(e.getY());
//                scale.setX(2);
//                scale.setY(2);
//                innerPane.getTransforms().add(scale);

//                workspace.setClip(new Rectangle(workspace.getWidth(), workspace.getHeight()));
                Point2D center = innerPane.localToParent(e.getX(), e.getY());
                Bounds bounds = innerPane.getBoundsInParent();

                double w = bounds.getWidth();
                double h = bounds.getHeight();

                double dw = w * (1.5 - 1);
                double xr = 2 * (w / 2 - (center.getX() - bounds.getMinX())) / w;

                double dh = h * (1.5 - 1);
                double yr = 2 * (h / 2 - (center.getY() - bounds.getMinY())) / h;

                innerPane.setScaleX(innerPane.getScaleX() * 1.5);
                innerPane.setScaleY(innerPane.getScaleY() * 1.5);
                innerPane.setTranslateX(innerPane.getTranslateX() + xr * dw / 2);
                innerPane.setTranslateY(innerPane.getTranslateY() + yr * dh / 2);
            }

            if (e.isSecondaryButtonDown()) {

                FlowPane flow = (FlowPane) app.getGUI().getAppPane().getTop();

                innerPane.setScaleX(innerPane.getScaleX() * 0.6);
                innerPane.setScaleY(innerPane.getScaleY() * 0.6);

                innerPane.setTranslateX(0.0);
                innerPane.setTranslateY(0 - flow.getHeight());

            }
        });

        app.getGUI().getAppPane().setOnKeyPressed(eh -> {

            DataManager dataManager = (DataManager) app.getDataComponent();

            workspace.setClip(new Rectangle(workspace.getWidth(), workspace.getHeight()));

            if (eh.getCode().toString().equals("RIGHT")) {

                innerPane.setTranslateX(innerPane.getTranslateX() - 10);

            } else if (eh.getCode().toString().equals("LEFT")) {

                innerPane.setTranslateX(innerPane.getTranslateX() + 10);

            } else if (eh.getCode().toString().equals("UP")) {

                innerPane.setTranslateY(innerPane.getTranslateY() + 10);
            } else if (eh.getCode().toString().equals("DOWN")) {

                innerPane.setTranslateY(innerPane.getTranslateY() - 10);
            }

            if (eh.getCode().toString().equals("G")) {

                if (G == false) {

                    FlowPane flow = (FlowPane) app.getGUI().getAppPane().getTop();

                    double width = app.getWorkspaceComponent().getWorkspace().getWidth();
                    double height = app.getGUI().getAppPane().getHeight() - flow.getHeight();

                    Line timeline = new Line();
                    timeline.setStroke(Color.WHITE);
                    timeline.setStartX(0);
                    timeline.setEndX(0);
                    timeline.setStartY(0);
                    timeline.setEndY(height);
                    innerPane.getChildren().add(timeline);

                    Line line = new Line();
                    line.setStroke(Color.WHITE);
                    line.setStartX(0);
                    line.setEndX(width);
                    line.setStartY(height / 2);
                    line.setEndY(height / 2);
                    innerPane.getChildren().add(line);

                    Line line2 = new Line();
                    line2.setStroke(Color.WHITE);
                    line2.setStartX(width / 2);
                    line2.setEndX(width / 2);
                    line2.setStartY(0);
                    line2.setEndY(height);
                    innerPane.getChildren().add(line2);

                    int temp = -180;
                    double relativeX;
                    double relativeY;

                    for (int i = -180; i <= 180; i += 30) {

                        relativeX = dataManager.convertX(i);
                        Line line3 = new Line();
                        line3.setStyle("-fx-stroke-dash-array: 10;");
                        line3.setStroke(Color.WHITE);
                        line3.setStartX(relativeX);
                        line3.setEndX(relativeX);
                        line3.setStartY(0);
                        line3.setEndY(height);

                        innerPane.getChildren().add(line3);
                    }

                    for (int i = -90; i <= 90; i += 30) {

                        relativeY = dataManager.convertY(i);
                        Line line3 = new Line();
                        line3.setStyle("-fx-stroke-dash-array: 10;");
                        line3.setStroke(Color.WHITE);
                        line3.setStartX(0);
                        line3.setEndX(width);
                        line3.setStartY(relativeY);
                        line3.setEndY(relativeY);
                        innerPane.getChildren().add(line3);
                    }
                    
                    G = true;
                } else if (G == true) {
                    innerPane.getChildren().remove(innerPane.getChildren().size() - 23, innerPane.getChildren().size());
                    G = false;
                }
            }
        });
    }
    
    @Override
    public void initStyle() {
        ((FlowPane) app.getGUI().getAppPane().getTop()).getChildren().remove(0);
        ((FlowPane) app.getGUI().getAppPane().getTop()).getChildren().remove(1);
    }

    public Pane getInnerPane() {

        return innerPane;
    }

    public void changeG(boolean value) {

        G = false;
    }
}
